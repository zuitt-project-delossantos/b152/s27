let http = require("http");

//Mock Data for Users
let users = [

	{
		username: "moonknight1999",
		email: "moonGod@gmail.com",
		password: "moonKnightStrong"
	},
	{
		username: "kitkatMachine",
		email: "notSponsored@gmail.com",
		password: "kitkatForever"

	}


];

let courses = [

	{
		name: "Science 101",
		price: 2500,
		isActive: true
	},
	{
		name: "English 101",
		price: 2500,
		isActive: true
	}
	
];

http.createServer((req,res)=>{



	} else if(req.url === "/users" && req.method === "GET"){

		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end("Hello, User! You are getting the details for our users.")
		//Change the value of Content-Type header if were passing json as our server response: 'application/json'
		res.writeHead(200,{'Content-Type': 'application/json'});
		//We cannot pass other data type as response except for strings.
		//To be able to pass the array of users, first we stringify the array as JSON.
		res.end(JSON.stringify(users));

	} else if(req.url === "/users" && req.method === "POST"){

		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("Hello, User! You are creating a new user.")
		//This route should allow us to receive data input from our client and add that data as a new user object in our users array.

		//This will act as a placeholder for the body of our POST request.
		let requestBody = "";

		//Receiving data from a client to a nodejs server requires 2 steps:

		//data step - this part wll read the stream of data from our client and process the incoming data into the requestBody variable
		req.on('data', function(data){

			/*console.log(data);*/
			requestBody += data;
			

		})

		//end step - this will run once or after the request has been completely sent from our client.
		req.on('end',function(){

			//requestBody now contains the data from our Postman client
			//However it is in JSON format. To process our request body we have to parse it into a proper JS Object using JSON.parse
			console.log(requestBody);
			requestBody = JSON.parse(requestBody);

			//Simulate creating a document and adding into a collection
			let newUser = {

				username: requestBody.username,
				email: requestBody.email,
				password: requestBody.password

			}

			users.push(newUser);
			console.log(users);

			res.writeHead(200,{'Content-Type':'application/json'});
			res.end(JSON.stringify(users));

		})


	} else if(req.url === "/courses" && req.method === "GET"){

		res.writeHead(200,{'Content-Type': 'application/json'});
		res.end(JSON.stringify(courses));
	}

	
	/*
		Mini-Activity:

		Create a new route with POST method.

		This route is on "/courses" endpoint and it is a POST method request.
			-Status code: 200
			-Headers: Content-Type: application/json
			-Receive the request body from our Postman client with our 2 step functions data step and end step.
			-Simulate the creation of a new courses document and add the new object into the courses array
			-End the response with end() inside the req.on('end') and send the updated courses array as JSON into our client.
			-Use the post method route for user creation as reference.

		-Take a screenshot of the response in postman and send it to our hangouts.

	*/


	/*
		Mini-Activity:

		Create a new route with GET method.

		This route is on "/courses" endpoint and it is a GET method request.
			-Status code: 200
			-Headers: Content-Type: application/json
			-End the response with end() and send the courses array as JSON into our client.

		-Take a screenshot of the response in postman and send it to our hangouts.

	*/

	/*
		Mini-Activity:
